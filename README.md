# pkgdiff

*pkgdiff* is a Linux command-line tool to compare the packages installed on your Debian-like system with the distribution defaults.

## Usage

```bash
$ pkgdiff --help
usage: pkgdiff.py [-h] [--confdir CONFDIR] [--distrib DISTRIB] [--quiet]

Compare installed packages with distribution defaults

optional arguments:
  -h, --help         show this help message and exit
  --confdir CONFDIR  directory containing distributions informations
  --distrib DISTRIB  distribution target
  --quiet            only display diff
```

## Output

Like the *diff* command-line tool, the lines of the output if prefixed with `<` or `>`. Consider the packages list on your system as the *from*, and the packages list of the distribution as the *to*. So:
- Lines beginning with `<` are for packages that came natively with your distribution, and that had been removed.
- Lines beginning with `>` are for packages installed besides the defaults ones.

## Installation

If you have root access,
- Put *pkgdiff* somewhere in your PATH's directory (`/usr/bin/` is a good place),
- Put the whole *conf* directory in `/etc/`, and rename it `pkgdiff`.
Your usage will be:
```bash
$ pkgdiff
```

If you are not root,
- Put *pkgdiff* somewhere in your home directory (`~/bin/` is a good place), and be sure this directory is in you PATH,
- Put the whole *conf* directory in your home directory (`~/.config/` is a standard), and rename it `pkgdiff`.
Your usage will be:
```bash
$ pkgdiff --confdir ~/.config/pkgdiff
```

## What is that for?

Let me be clear: I made this tool because I am an obsessive cleaning guy. Sometimes I need to install packages to test some stuff. Few minutes, hours or days after that, I do not need them anymore. But those packages are still there, taking up disk space, consumming network bandwith, CPU, and time to keep them up-to-date. This tool helps me to identify those useless packages.

Another reason is to check cloud providers distributions. When you get a VM or dedicated server, it is installed with an operating system by your cloud provider. *pkgdiff* will show you if packages had been missed or added, helping you to be consistent between providers (and also satisfying your curiosity).
